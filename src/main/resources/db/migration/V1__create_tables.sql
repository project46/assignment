USE ASSIGNMENT;

CREATE TABLE IF NOT EXISTS CAR (
  ID                        int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PRIMARY KEY',
  NAME                      varchar(50) NOT NULL COMMENT 'NAME OF THE CAR',
  MANUFACTURE_NAME          varchar(50) NOT NULL COMMENT 'MANUFACTURE NAME OF THE CAR',
  MODEL                     varchar(50) NOT NULL COMMENT 'MODEL OF THE CAR',
  MANUFACTURING_YEAR        int(10) unsigned NOT NULL COMMENT 'MANUFACTURE YEAR OF THE CAR',
  COLOR                     varchar(50) NOT NULL COMMENT 'COLOR OF THE CAR',
  CONSTRAINT PK_CAR_ID PRIMARY KEY (ID)
);
GRANT SELECT,INSERT ON CAR TO admin;