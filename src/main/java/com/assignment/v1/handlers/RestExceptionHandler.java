package com.assignment.v1.handlers;

import com.assignment.v1.constant.Constants;
import com.assignment.v1.response.ApiResponse;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Objects;

import static com.assignment.v1.constant.Constants.COMMA_SPACE;


/**
 * Created by Anshuman on 22/03/20.
 * this class is used to handle errors
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  private static final String MALFORMED_JSON_REQUEST = "Malformed JSON request";
  private static final String SHOULD_BE_TYPE_OF = " should be of type of ";
  private static final String MISSING_PARAMETER = " parameter is missing";
  private static final String CONSTRAINT_VIOLATION =
      "Constraint violations for the below constraint/s: \n";
  private static final String METHOD_NOT_SUPPORTED =
      " method is not supported for this request. Supported methods are ";
  private static final String MEDIA_TYPE_NOT_SUPPORTED =
      " media type is not supported. Supported media types are ";
  private static final String NO_HANDLER_FOUND = "No handler found for ";

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    return buildResponseEntity(new ApiResponse(HttpStatus.BAD_REQUEST, MALFORMED_JSON_REQUEST, ex));
  }

  private ResponseEntity<Object> buildResponseEntity(ApiResponse apiResponse) {
    return new ResponseEntity<>(apiResponse, apiResponse.getStatus());
  }

  @Override
  protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
      HttpStatus status, WebRequest request) {
    ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST);
    String sb =
        String.format("%s%s%s", ex.getPropertyName(), SHOULD_BE_TYPE_OF, ex.getRequiredType());
    apiResponse.setMessage(sb);
    return buildResponseEntity(apiResponse);
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST);
    String sb = String.format("%s%s", ex.getParameterName(), MISSING_PARAMETER);
    apiResponse.setMessage(sb);
    return buildResponseEntity(apiResponse);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex) {
    ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, ex);
    StringBuilder sb = new StringBuilder();
    sb.append(CONSTRAINT_VIOLATION);
    for (ConstraintViolation<?> constraintViolation : ex.getConstraintViolations()) {
      sb.append(constraintViolation.getRootBeanClass().getName());
      sb.append(Constants.EMPTY_SPACE);
      sb.append(constraintViolation.getPropertyPath());
      sb.append(Constants.COLON);
      sb.append(Constants.EMPTY_SPACE);
      sb.append(constraintViolation.getMessage());
      sb.append(Constants.NEW_LINE);
    }
    apiResponse.setMessage(sb.toString());
    return buildResponseEntity(apiResponse);
  }

  @Override
  protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    ApiResponse apiResponse = new ApiResponse(HttpStatus.NOT_FOUND, ex);
    String sb =
        String.format("%s%s%s", NO_HANDLER_FOUND, Constants.EMPTY_SPACE, ex.getRequestURL());
    apiResponse.setMessage(sb);
    return buildResponseEntity(apiResponse);
  }

  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
      HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    ApiResponse apiResponse = new ApiResponse(HttpStatus.METHOD_NOT_ALLOWED, ex);
    StringBuilder builder = new StringBuilder();
    builder.append(ex.getMethod());
    builder.append(METHOD_NOT_SUPPORTED);
    Objects.requireNonNull(ex.getSupportedHttpMethods())
        .forEach(t -> builder.append(t).append(Constants.EMPTY_SPACE));
    apiResponse.setMessage(builder.toString());
    return buildResponseEntity(apiResponse);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
      HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    ApiResponse apiResponse = new ApiResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE, ex);
    StringBuilder builder = new StringBuilder();
    builder.append(ex.getContentType());
    builder.append(MEDIA_TYPE_NOT_SUPPORTED);
    ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(COMMA_SPACE));
    apiResponse.setMessage(builder.substring(0, builder.length() - 2));
    return buildResponseEntity(apiResponse);
  }


  @ExceptionHandler(Exception.class)
  protected ResponseEntity<Object> handleException(Exception ex) {
    ApiResponse apiResponse = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex);
    return buildResponseEntity(apiResponse);
  }
}
