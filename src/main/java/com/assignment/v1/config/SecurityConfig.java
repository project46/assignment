package com.assignment.v1.config;

import com.assignment.v1.constant.ApiAttributeConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.assignment.v1.constant.ApiAttributeConstants.END_POINT_PREFIX;

@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${security.user.username}")
  private String userUserName;

  @Value("${security.user.pasword}")
  private String userPassword;

  @Value("${security.admin.username}")
  private String adminUsername;

  @Value("${security.admin.password}")
  private String adminPassword;

  @Override
  public void configure(AuthenticationManagerBuilder auth) {
    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    try {
      auth.inMemoryAuthentication().withUser(userUserName).password(encoder.encode(userPassword))
          .roles(ApiAttributeConstants.USER).and().withUser(adminUsername)
          .password(encoder.encode(adminPassword))
          .roles(ApiAttributeConstants.USER, ApiAttributeConstants.ADMIN);
    } catch (Exception e) {
      log.error("Error while configuring security", e);
    }
  }

  @Override
  public void configure(HttpSecurity http) {
    try {
      http.httpBasic().and().formLogin().and().authorizeRequests().antMatchers(HttpMethod.GET, END_POINT_PREFIX)
          .hasRole(ApiAttributeConstants.USER).antMatchers(HttpMethod.POST, END_POINT_PREFIX)
          .hasRole(ApiAttributeConstants.ADMIN).antMatchers(HttpMethod.PUT, END_POINT_PREFIX)
          .hasRole(ApiAttributeConstants.ADMIN).antMatchers(HttpMethod.PATCH, END_POINT_PREFIX)
          .hasRole(ApiAttributeConstants.ADMIN).antMatchers(HttpMethod.DELETE, END_POINT_PREFIX)
          .hasRole(ApiAttributeConstants.ADMIN).and().csrf().disable();
    } catch (Exception e) {
      log.error("Error while configuring security", e);
    }
  }
}