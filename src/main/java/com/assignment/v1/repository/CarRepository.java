package com.assignment.v1.repository;

import com.assignment.v1.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Anshuman on 22/03/20.
 */
@Repository("com.assignment.v1.repository.CarRepository")
public interface CarRepository extends JpaRepository<Car, Long> {
  Car findByName(String name);

  Car findByColor(String color);

  Car findByManufacturingYear(Long manufacturingYear);

  Car findByModel(String model);

  Car findByManufactureName(String manufactureName);
}
