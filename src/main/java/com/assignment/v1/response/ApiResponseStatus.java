package com.assignment.v1.response;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by Anshuman on 22/03/20.
 */
public enum ApiResponseStatus {
  SUCCESS(1000, "SUCCESS"),
  FAILED(9000, "FAILED");

  private final int code;
  private final String message;

  ApiResponseStatus(int code, String message) {
    this.code = code;
    this.message = message;
  }

  @JsonValue
  protected ApiResponseStatusToJson toJson() {
    ApiResponseStatusToJson apiResponseStatusToJson = new ApiResponseStatusToJson();
    apiResponseStatusToJson.setCode(this.code);
    apiResponseStatusToJson.setMessage(this.message);
    return apiResponseStatusToJson;
  }
}
