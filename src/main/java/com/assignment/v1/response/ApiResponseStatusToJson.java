package com.assignment.v1.response;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anshuman on 22/03/20.
 */
@Setter
@Getter
class ApiResponseStatusToJson {
  private int code;
  private String message;
}
