package com.assignment.v1.response;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

/**
 * Created by Anshuman on 22/03/20.
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse<T> {
  private static final String DATE_PATTERN = "dd-MM-yyyy hh:mm:ss";
  private static final String UNEXPECTED_ERROR = "UnExpected Error";
  private HttpStatus status;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_PATTERN)
  private LocalDateTime timeStamp;
  private String message;
  private String debugMessage;
  private T responseObject;
  private ApiResponseStatus apiResponseStatus;

  private ApiResponse() {
    timeStamp = LocalDateTime.now();
  }

  public ApiResponse(ApiResponseStatus apiResponseStatus, T responseObject) {
    this();
    this.apiResponseStatus = apiResponseStatus;
    this.responseObject = responseObject;
  }

  public ApiResponse(HttpStatus status) {
    this();
    this.status = status;
  }

  public ApiResponse(HttpStatus status, Throwable exception) {
    this();
    this.status = status;
    this.message = UNEXPECTED_ERROR;
    this.debugMessage = exception.getLocalizedMessage();
  }

  public ApiResponse(HttpStatus status, String message, Throwable exception) {
    this();
    this.status = status;
    this.message = message;
    this.debugMessage = exception.getLocalizedMessage();
  }
}
