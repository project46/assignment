package com.assignment.v1.entity;

import com.assignment.v1.constant.AttributeConstants;
import com.assignment.v1.constant.EntityConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Anshuman on 22/03/20.
 */
@Setter
@Getter
@Entity
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@ToString(doNotUseGetters = true)
@EqualsAndHashCode(doNotUseGetters = true)
@Table(name = EntityConstants.CAR)
public class Car implements Serializable {
  private static final long serialVersionUID = -6532224343706614882L;

  @Id
  @Column(name = "ID", nullable = false, unique = true)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = AttributeConstants.NAME, nullable = false)
  @NotNull
  private String name;

  @Column(name = AttributeConstants.MANUFACTURE_NAME, nullable = false)
  @NotNull
  private String manufactureName;

  @Column(name = AttributeConstants.MODEL, nullable = false)
  @NotNull
  private String model;

  @Column(name = AttributeConstants.MANUFACTURING_YEAR, nullable = false)
  @NotNull
  private Long manufacturingYear;

  @Column(name = AttributeConstants.COLOR, nullable = false)
  @NotNull
  private String color;
}

