package com.assignment.v1.constant;

/**
 * Created by Anshuman on 22/03/20.
 */
public final class ApiAttributeConstants {
  private ApiAttributeConstants() {
  }

  public static final String NAME = "name";
  public static final String MANUFACTURE_NAME = "manufactureName";
  public static final String MODEL = "model";
  public static final String MANUFACTURING_YEAR = "manufacturingYear";
  public static final String COLOR = "color";
  public static final String ID = "id";
  public static final String END_POINT_PREFIX = "/car/**";
  public static final String USER = "USER";
  public static final String ADMIN = "ADMIN";
}
