package com.assignment.v1.constant;

/**
 * Created by Anshuman on 22/03/20.
 */
public final class AttributeConstants {
  private AttributeConstants() {
  }

  public static final String NAME = "NAME";
  public static final String MANUFACTURE_NAME = "MANUFACTURE_NAME";
  public static final String MODEL = "MODEL";
  public static final String MANUFACTURING_YEAR = "MANUFACTURING_YEAR";
  public static final String COLOR = "COLOR";

}
