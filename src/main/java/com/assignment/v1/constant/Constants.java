package com.assignment.v1.constant;

/**
 * Created by Anshuman on 22/03/20.
 */
public final class Constants {
  private Constants() {
  }

  public static final String EMPTY_SPACE = " ";
  public static final String COLON = ":";
  public static final String NEW_LINE = "\n";
  public static final String COMMA_SPACE = ", ";
}
