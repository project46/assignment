package com.assignment.v1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Anshuman on 22/03/20.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseModel {
  @JsonIgnore
  protected Long id;
}
