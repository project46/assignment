package com.assignment.v1.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Created by Anshuman on 22/03/20.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@EqualsAndHashCode(callSuper = true, doNotUseGetters = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString(doNotUseGetters = true)
public class CarDetails extends BaseModel {

  @NotNull(message = "NAME CANNOT BE NULL")
  private String name;

  @NotNull(message = "MANUFACTURE_NAME CANNOT BE NULL")
  private String manufactureName;

  @NotNull(message = "MODEL CANNOT BE NULL")
  private String model;

  @NotNull(message = "MANUFACTURING_YEAR CANNOT BE NULL")
  private Long manufacturingYear;

  @NotNull(message = "COLOR CANNOT BE NULL")
  private String color;
}
