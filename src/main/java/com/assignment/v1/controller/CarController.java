package com.assignment.v1.controller;

import com.assignment.v1.constant.ApiAttributeConstants;
import com.assignment.v1.model.CarDetails;
import com.assignment.v1.response.ApiResponse;
import com.assignment.v1.service.CarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Anshuman on 22/03/20.
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/car", produces = {MediaType.APPLICATION_JSON_VALUE})
@Slf4j
public class CarController {

  @Autowired
  @Qualifier("com.assignment.v1.service.implementation.CarServiceImplementation")
  private CarService carService;

  @PostMapping(value = "/save")
  public ResponseEntity<ApiResponse<CarDetails>> save(@RequestBody() CarDetails carDetails) {
    ApiResponse<CarDetails> carApiResponse = carService.save(carDetails);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @PutMapping(value = "/update/{id}")
  public ResponseEntity<ApiResponse<CarDetails>> update(
      @PathVariable(ApiAttributeConstants.ID) long id, @RequestBody() CarDetails carDetails) {
    ApiResponse<CarDetails> carApiResponse = carService.updateById(carDetails, id);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @GetMapping(value = "/findAll")
  public ResponseEntity<ApiResponse<List<CarDetails>>> findAll() {
    ApiResponse<List<CarDetails>> carApiResponse = carService.findAll();
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @GetMapping(value = "id/{id}")
  public ResponseEntity<ApiResponse<CarDetails>> findById(
      @PathVariable(ApiAttributeConstants.ID) long id) {
    ApiResponse<CarDetails> carApiResponse = carService.findById(id);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @GetMapping(value = "name/{name}")
  public ResponseEntity<ApiResponse<CarDetails>> findByName(
      @PathVariable(ApiAttributeConstants.NAME) String name) {
    ApiResponse<CarDetails> carApiResponse = carService.findByName(name);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @GetMapping(value = "manufactureName/{manufactureName}")
  public ResponseEntity<ApiResponse<CarDetails>> findByManufactureName(
      @PathVariable(ApiAttributeConstants.MANUFACTURE_NAME) String manufactureName) {
    ApiResponse<CarDetails> carApiResponse = carService.findByManufactureName(manufactureName);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @GetMapping(value = "model/{model}")
  public ResponseEntity<ApiResponse<CarDetails>> findByModel(
      @PathVariable(ApiAttributeConstants.MODEL) String model) {
    ApiResponse<CarDetails> carApiResponse = carService.findByModel(model);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @GetMapping(value = "manufacturingYear/{manufacturingYear}")
  public ResponseEntity<ApiResponse<CarDetails>> findBymanufacturingYear(
      @PathVariable(ApiAttributeConstants.MANUFACTURING_YEAR) long manufacturingYear) {
    ApiResponse<CarDetails> carApiResponse = carService.findByManufacturingYear(manufacturingYear);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @GetMapping(value = "color/{color}")
  public ResponseEntity<ApiResponse<CarDetails>> findByColor(
      @PathVariable(ApiAttributeConstants.COLOR) String color) {
    ApiResponse<CarDetails> carApiResponse = carService.findByColor(color);
    return new ResponseEntity<>(carApiResponse, HttpStatus.OK);
  }

  @DeleteMapping(value = "/delete/{id}")
  public void deleteById(@PathVariable(ApiAttributeConstants.ID) long id) {
    carService.deleteById(id);
  }

  @DeleteMapping(value = "/deleteAll")
  public void deleteAll() {
    carService.deleteAll();
  }
}
