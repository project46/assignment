package com.assignment.v1.service;

import com.assignment.v1.model.CarDetails;
import com.assignment.v1.response.ApiResponse;

import java.util.List;

/**
 * Created by Anshuman on 22/03/20.
 */
public interface CarService {
  ApiResponse<List<CarDetails>> findAll();

  ApiResponse<CarDetails> findById(long carId);

  ApiResponse<CarDetails> save(CarDetails carDetails);

  ApiResponse<CarDetails> updateById(CarDetails carDetails, long carId);

  void deleteById(long carId);

  void deleteAll();

  ApiResponse<CarDetails> findByName(String name);

  ApiResponse<CarDetails> findByManufactureName(String manufactureName);

  ApiResponse<CarDetails> findByModel(String model);

  ApiResponse<CarDetails> findByManufacturingYear(Long manufacturingYear);

  ApiResponse<CarDetails> findByColor(String color);
}
