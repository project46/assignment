package com.assignment.v1.service.implementation;

import com.assignment.v1.entity.Car;
import com.assignment.v1.model.CarDetails;
import com.assignment.v1.repository.CarRepository;
import com.assignment.v1.response.ApiResponse;
import com.assignment.v1.service.CarService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.assignment.v1.response.ApiResponseStatus.FAILED;
import static com.assignment.v1.response.ApiResponseStatus.SUCCESS;

/**
 * Created by Anshuman on 22/03/20.
 */
@Service("com.assignment.v1.service.implementation.CarServiceImplementation")
@Slf4j
public class CarServiceImplementation implements CarService {

  @Autowired
  @Qualifier("com.assignment.v1.repository.CarRepository")
  private CarRepository carRepository;

  /**
   * find all cars
   *
   * @return list of car
   */
  @Override
  public ApiResponse<List<CarDetails>> findAll() {
    ApiResponse<List<CarDetails>> apiResponse;
    List<Car> cars = carRepository.findAll();
    log.info("Finding all carDetails");
    if (CollectionUtils.isNotEmpty(cars)) {
      List<CarDetails> carDetailsList = cars.stream().map(
          car -> CarDetails.builder().color(car.getColor())
              .manufactureName(car.getManufactureName())
              .manufacturingYear(car.getManufacturingYear()).model(car.getModel())
              .name(car.getName()).build()).collect(Collectors.toList());
      apiResponse = new ApiResponse<>(SUCCESS, carDetailsList);
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, null);
      log.info("No record found for carDetails");
    }
    return apiResponse;
  }

  /**
   * find car by id
   *
   * @param carId :  car id
   *
   * @return car details
   */
  public ApiResponse<CarDetails> findById(long carId) {
    ApiResponse<CarDetails> apiResponse;
    Optional<Car> carDb = carRepository.findById(carId);
    log.info("Finding carDetails for carId: {}", carId);
    if (carDb.isPresent()) {
      Car car = carDb.get();
      CarDetails carDetails =
          CarDetails.builder().color(car.getColor()).manufactureName(car.getManufactureName())
              .manufacturingYear(car.getManufacturingYear()).model(car.getModel())
              .name(car.getName()).build();
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, null);
      log.info("No record found for carDetails for carId: {}", carId);
    }
    return apiResponse;
  }

  /**
   * saving car details
   *
   * @param carDetails : car details
   *
   * @return car details
   */
  @Override
  public ApiResponse<CarDetails> save(CarDetails carDetails) {
    ApiResponse<CarDetails> apiResponse = new ApiResponse<>(FAILED, carDetails);
    Car car = new Car();
    car.setColor(carDetails.getColor());
    car.setManufactureName(carDetails.getManufactureName());
    car.setManufacturingYear(carDetails.getManufacturingYear());
    car.setModel(carDetails.getModel());
    car.setName(carDetails.getName());
    Car carDb = carRepository.save(car);
    if (null != carDb) {
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
      log.info("Success saved operation on carDetails");
    } else {
      log.info("Failed saved operation on carDetails");
    }
    return apiResponse;
  }


  /**
   * saving car details
   *
   * @param carDetails : car details
   *
   * @return car details
   */
  @Override
  public ApiResponse<CarDetails> updateById(CarDetails carDetails, long carId) {
    ApiResponse<CarDetails> apiResponse = new ApiResponse<>(FAILED, carDetails);
    Optional<Car> carDb = carRepository.findById(carId);
    if (carDb.isPresent()) {
      Car car = carDb.get();
      car.setColor(carDetails.getColor());
      car.setManufactureName(carDetails.getManufactureName());
      car.setManufacturingYear(carDetails.getManufacturingYear());
      car.setModel(carDetails.getModel());
      car.setName(carDetails.getName());
      Car updatedCarDb = carRepository.save(car);
      if (null != updatedCarDb) {
        apiResponse = new ApiResponse<>(SUCCESS, carDetails);
        log.info("Success update operation on carDetails");
      } else {
        log.info("Failed update operation on carDetails");
      }
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
      log.info("No record found for carId: {}", carId);
    }
    return apiResponse;
  }

  /**
   * delete car by id
   *
   * @param carId : car id
   */
  @Override
  public void deleteById(long carId) {
    log.info("Deleting records from car for id: {}", carId);
    Optional<Car> carDb = carRepository.findById(carId);
    if (carDb.isPresent()) {
      carRepository.deleteById(carId);
    } else {
      log.info("No such carDetails present for carId: {}", carId);
    }
  }

  /**
   * delete all cars
   */
  @Override
  public void deleteAll() {
    log.info("Deleting all records");
    carRepository.deleteAll();
  }

  /**
   * find car by name
   *
   * @param name : name of the car
   *
   * @return : car details
   */
  @Override
  public ApiResponse<CarDetails> findByName(String name) {
    ApiResponse<CarDetails> apiResponse;
    Car car = carRepository.findByName(name);
    log.info("Finding carDetails for name: {}", name);
    if (null != car) {
      CarDetails carDetails =
          CarDetails.builder().color(car.getColor()).manufactureName(car.getManufactureName())
              .manufacturingYear(car.getManufacturingYear()).model(car.getModel())
              .name(car.getName()).build();
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, null);
      log.info("No record found for carDetails for name: {}", name);
    }
    return apiResponse;
  }

  /**
   * find car by manufactureName
   *
   * @param manufactureName : manufactureName of the car
   *
   * @return : car details
   */
  @Override
  public ApiResponse<CarDetails> findByManufactureName(String manufactureName) {
    ApiResponse<CarDetails> apiResponse;
    Car car = carRepository.findByManufactureName(manufactureName);
    log.info("Finding carDetails for manufactureName: {}", manufactureName);
    if (null != car) {
      CarDetails carDetails =
          CarDetails.builder().color(car.getColor()).manufactureName(car.getManufactureName())
              .manufacturingYear(car.getManufacturingYear()).model(car.getModel())
              .name(car.getName()).build();
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, null);
      log.info("No record found for carDetails for manufactureName: {}", manufactureName);
    }
    return apiResponse;
  }

  /**
   * find car by model
   *
   * @param model : model of the car
   *
   * @return : car details
   */
  @Override
  public ApiResponse<CarDetails> findByModel(String model) {
    ApiResponse<CarDetails> apiResponse;
    Car car = carRepository.findByModel(model);
    log.info("Finding carDetails for model: {}", model);
    if (null != car) {
      CarDetails carDetails =
          CarDetails.builder().color(car.getColor()).manufactureName(car.getManufactureName())
              .manufacturingYear(car.getManufacturingYear()).model(car.getModel())
              .name(car.getName()).build();
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, null);
      log.info("No record found for carDetails for model: {}", model);
    }
    return apiResponse;
  }

  /**
   * find car by manufacturingYear
   *
   * @param manufacturingYear : manufacturingYear of the car
   *
   * @return : car details
   */
  @Override
  public ApiResponse<CarDetails> findByManufacturingYear(Long manufacturingYear) {
    ApiResponse<CarDetails> apiResponse;
    Car car = carRepository.findByManufacturingYear(manufacturingYear);
    log.info("Finding carDetails for manufacturingYear: {}", manufacturingYear);
    if (null != car) {
      CarDetails carDetails =
          CarDetails.builder().color(car.getColor()).manufactureName(car.getManufactureName())
              .manufacturingYear(car.getManufacturingYear()).model(car.getModel())
              .name(car.getName()).build();
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, null);
      log.info("No record found for carDetails for manufacturingYear: {}", manufacturingYear);
    }
    return apiResponse;
  }

  /**
   * find car by color
   *
   * @param color : name of the color
   *
   * @return : car details
   */
  @Override
  public ApiResponse<CarDetails> findByColor(String color) {
    ApiResponse<CarDetails> apiResponse;
    Car car = carRepository.findByColor(color);
    log.info("Finding carDetails for color: {}", color);
    if (null != car) {
      CarDetails carDetails =
          CarDetails.builder().color(car.getColor()).manufactureName(car.getManufactureName())
              .manufacturingYear(car.getManufacturingYear()).model(car.getModel())
              .name(car.getName()).build();
      apiResponse = new ApiResponse<>(SUCCESS, carDetails);
    } else {
      apiResponse = new ApiResponse<>(SUCCESS, null);
      log.info("No record found for carDetails for color: {}", color);
    }
    return apiResponse;
  }
}
